# Game of Life

[Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) implementation using [p5.js](https://p5js.org/).

## Run locally

```sh
pnpm run dev
```

## Controls

- Mouse click or drag: draw in canvas
- `SPACE`: start/stop simulation
- `DELETE`: clear canvas (only in drawing mode)
- `+`: Speed up
- `-`: Slow down
