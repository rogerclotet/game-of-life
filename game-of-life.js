const canvasSize = 600;

let grid = [];
const gridSize = 80;
const cellSize = canvasSize / gridSize;

let lastep = 0;
let stepMillis = 200;

const STATE_DRAWING = "drawing";
const STATE_RUNNING = "running";
let state = STATE_DRAWING;

const DRAWING_STROKE_COLOR = "#b3e8f0";
const RUNNING_STROKE_COLOR = "#cceecc";

function setup() {
  const canvas = document.getElementById("sketch");
  createCanvas(canvasSize, canvasSize, "p2d", canvas);

  background(255);

  for (let i = 0; i < gridSize; i++) {
    grid[i] = [];
    for (let j = 0; j < gridSize; j++) {
      grid[i][j] = false;
    }
  }

  lastStep = millis();
}

function countNeighbors(x, y) {
  let neighbors = 0;
  for (let i of [-1, 0, 1]) {
    for (let j of [-1, 0, 1]) {
      if (i === 0 && j === 0) {
        continue;
      }

      const row = (x + i + gridSize) % gridSize;
      const col = (y + j + gridSize) % gridSize;

      if (grid[row][col]) {
        neighbors++;
      }
    }
  }
  return neighbors;
}

function step() {
  const nextGrid = [];
  for (let i = 0; i < gridSize; i++) {
    nextGrid[i] = [];
    for (let j = 0; j < gridSize; j++) {
      const neighbors = countNeighbors(i, j);
      if (grid[i][j]) {
        nextGrid[i][j] = neighbors === 2 || neighbors === 3;
      } else {
        nextGrid[i][j] = neighbors === 3;
      }
    }
  }

  grid = nextGrid;
}

function draw() {
  if (state === STATE_RUNNING && millis() >= lastStep + stepMillis) {
    step();
    lastStep = millis();
  }

  if (state === STATE_DRAWING) {
    stroke(DRAWING_STROKE_COLOR);
  } else {
    stroke(RUNNING_STROKE_COLOR);
  }

  for (let i = 0; i < gridSize; i++) {
    for (let j = 0; j < gridSize; j++) {
      if (grid[i][j]) {
        fill(0);
      } else {
        fill(255);
      }
      rect(j * cellSize, i * cellSize, cellSize, cellSize);
    }
  }
}

function mousePressed() {
  if (state === STATE_DRAWING) {
    const x = Math.floor(mouseY / cellSize);
    const y = Math.floor(mouseX / cellSize);
    grid[x][y] = !grid[x][y];
  }
}

function mouseDragged() {
  if (state === STATE_DRAWING) {
    const x = Math.floor(mouseY / cellSize);
    const y = Math.floor(mouseX / cellSize);
    grid[x][y] = true;
  }
}

function keyPressed() {
  if (key === " ") {
    state = state === STATE_RUNNING ? STATE_DRAWING : STATE_RUNNING;
  } else if (state === STATE_DRAWING) {
    if (keyCode === DELETE) {
      for (let i = 0; i < gridSize; i++) {
        for (let j = 0; j < gridSize; j++) {
          grid[i][j] = false;
        }
      }
    }
  } else if (state === STATE_RUNNING) {
    if (key === "+") {
      stepMillis = min(50, stepMillis - 50);
    } else if (key === "-") {
      stepMillis += 50;
    }
  }
}
